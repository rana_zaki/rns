import React from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import Spacing from '_styles';
const HomeScreen = () => (
  <SafeAreaView style={{flex: 1}}>
    <View
      style={{
        paddingHorizontal: Spacing.SCALE_12,
        alignSelf: 'center',
        flexDirection: 'row',
      }}>
      <Text>Recent Transactions</Text>
    </View>
  </SafeAreaView>
);

export default HomeScreen;

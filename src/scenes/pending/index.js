import React from 'react';
import {SafeAreaView, Text} from 'react-native';

const PendingScreen = () => (
  <SafeAreaView>
    <Text>Screen: pending</Text>
  </SafeAreaView>
);

export default PendingScreen;

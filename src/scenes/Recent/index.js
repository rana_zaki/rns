import React from 'react';
import {SafeAreaView, Text, View, Image, Dimensions} from 'react-native';
import Spacing from '_styles';
import BLUE from '_styles';
import {Transactions} from '_atoms';
import WebApi from '_services';
const RecentScreen = props => {
  const [page, setPage] = React.useState(1);
  const [data, setData] = React.useState([
    1,
    2,
    3,
    4,
    5,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
  ]);
  React.useEffect(() => {
    getData();
  }, [props.navigation]);

  const getData = async () => {
    console.log('load more');
    await new WebApi()
      .getRecentData(page)
      .then(res => {
        if (res.status == 200) {
          setData(data.concat(res.data));
        }
      })
      .catch(error => {
        console.log('error in get list', error.response);
      });
  };
  const loadNextPage = () => {
    setPage(page + 1);
    getData();
  };
  return (
    <SafeAreaView>
      <View
        style={{
          paddingHorizontal: 30,
          paddingTop: 15,
          alignSelf: 'center',
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '100%',
        }}>
        <Text>Recent Transactions</Text>
        <Image
          style={{width: 20, height: 20, tintColor: 'blue'}}
          resizeMode="contain"
          source={require('../../assets/images/filter.png')}
        />
      </View>
      <View style={{paddingHorizontal: 30}}>
        <Transactions data={data} handleLoadMore={() => loadNextPage()} />
      </View>
    </SafeAreaView>
  );
};
export default RecentScreen;

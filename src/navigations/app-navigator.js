import * as React from 'react';

import {NavigationContainer} from '@react-navigation/native';

import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import HomeScreen from '_scenes/home';
const Stack = createStackNavigator();
import MyTabs from './TabNavigator';
function AppNavigator(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={'MyTabs'}
        // independent={'true'}
        screenOptions={{
          headerShown: false,
          // gestureEnabled:true ,
          animationEnabled: true,
          ...TransitionPresets.ScaleFromCenterAndroid,
          // independent: true,
        }}>
        <Stack.Screen name="MyTabs" component={MyTabs} />
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{title: 'HomeScreen'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;

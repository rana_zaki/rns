import * as React from 'react';
import {StatusBar, SafeAreaView} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import SafeArea from 'react-native-safe-area-context';
import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import HomeScreen from '_scenes/home';
import AboutScreen from '_scenes/about';
import RecentScreen from '_scenes/Recent';
import PendingScreen from '_scenes/pending';
const Stack = createStackNavigator();
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      independent={'true'}
      tabBarOptions={{
        independent: true,
        style: {marginTop: 35},
        activeTintColor: 'blue',
      }}>
      <Tab.Screen name="Recent" component={RecentScreen} />
      <Tab.Screen name="Pending" component={PendingScreen} />
    </Tab.Navigator>
  );
}
export default MyTabs;

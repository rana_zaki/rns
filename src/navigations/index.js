import AuthNavigator from './auth-navigator';
import AppNavigator from './app-navigator';

export {default as MyTabs} from './TabNavigator';
export default AppNavigator;

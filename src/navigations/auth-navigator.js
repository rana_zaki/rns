import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  createStackNavigator,
  TransitionPresets,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import LoginScreen from '_scenes/login';

const Stack = createStackNavigator();

function AuthNavigator(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={'LoginScreen'}
        screenOptions={{
          headerShown: false,
          // gestureEnabled:true ,
          animationEnabled: true,
          ...TransitionPresets.ScaleFromCenterAndroid,
        }}>
        <Stack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{title: 'LoginScreen'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AuthNavigator;

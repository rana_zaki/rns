import axios from 'axios';
import {Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const BASE_URL = 'https://staging-backend.dafriexchange.com/'; // Staging URL
// const BASE_URL = 'https://staging-backend.dafriexchange.com/' //Live URL
const API_PARAMETER = '/api/';

export default class WebApi {
  // Below error message will be shown when api or internet is not working
  apiError() {
    Alert.alert(
      'Action Failed',
      'Something went wrong. Invalid email/password or check your internet connection and try again later.',
      [{text: 'OK', onPress: () => console.log('action failed')}],
    );
  }

  // GET Request
  async get(url) {
    let API_TOKEN = await AsyncStorage.getItem('userToken');
    // console.log("API_TOKEN: ", API_TOKEN)
    return axios.get(url, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${API_TOKEN}`,
      },
    });
  }

  // POST Request
  async post(url, data) {
    let API_TOKEN = await AsyncStorage.getItem('userToken');

    return axios.post(url, data, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${API_TOKEN}`,
      },
    });
  }

  getRecentData(page) {
    let url = BASE_URL + API_PARAMETER + 'admin/getAdminInquiryDetail';
    const data = {
      currentPage: page,
    };
    return this.post(url, data);
  }
}

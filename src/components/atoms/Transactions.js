import React from 'react';
import {
  View,
  Image,
  Platform,
  Dimensions,
  Modal,
  Alert,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  I18nManager,
  StyleSheet,
  ScrollView,
  Text,
  FlatList,
  Animated,
} from 'react-native';
import SCALE_12 from '_styles';
import BLUE from '_styles';

const Transactions = props => {
  const scrollY = React.useRef(new Animated.Value(0)).current;
  const [data, setData] = React.useState([1, 2, 3, 4, 5]);
  return (
    <FlatList
      data={props.data}
      key={item => item.key}
      removeClippedSubviews={true}
      contentContainerStyle={{
        marginBottom: 15,
        marginTop: 5,
        width: '80%',
        paddingHorizontal: 30,
        alignSelf: 'center',
      }}
      renderItem={({item, index}) => {
        return (
          <TouchableOpacity onPress={() => {}}>
            <Animated.View
              style={{
                flexDirection: 'row',
                paddingVertical: 30,
                paddingLeft: 15,
                flex: 1,
                height: 80,
                borderBottomWidth: 1,
                borderBottomColor: '#727272',
              }}>
              <View
                style={{
                  height: '100%',
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: '10%',
                }}>
                <View
                  style={{
                    backgroundColor: 'green',
                    borderRadius: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: 40,
                    width: 40,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 25,
                      fontWeight: 'bold',
                    }}>
                    +
                  </Text>
                </View>
              </View>
              <View
                style={{
                  width: '80%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  <Text
                    style={{color: 'blue', fontSize: 12, fontWeight: 'bold'}}>
                    bciqf
                  </Text>
                  <Text style={{color: 'gray', fontSize: 10}}>bciqf</Text>
                </View>
                <View>
                  <Text
                    style={{color: 'black', fontSize: 12, fontWeight: 'bold'}}>
                    25..55555
                  </Text>
                  <Text
                    style={{color: 'gray', fontSize: 10, textAlign: 'right'}}>
                    Tue 25,jan
                  </Text>
                </View>
              </View>
            </Animated.View>
          </TouchableOpacity>
        );
      }}
      keyExtractor={(item, index) => index.toString()}
      onEndReachedThreshold={0.2}
      onEndReached={() => props.handleLoadMore()}
      contentContainerStyle={{paddingBottom: 50}}
    />
  );
};
export default Transactions;

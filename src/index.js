import React from 'react';

import AppNavigator from '_navigations';

const App = () => <AppNavigator />;

export default App;
